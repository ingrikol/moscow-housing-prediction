import xgboost as xgb
import numpy as np
import pandas as pd
from utils import root_mean_squared_log_error
from sklearn.model_selection import StratifiedKFold


def xgb_preparation(train_data_in, test_data_in):
    train = train_data_in.copy()
    xgb_X = train.drop(columns=['id', 'price'])
    xgb_y = np.log(train.price / train.area_total)

    xgb_test = test_data_in.copy()
    xgb_test = xgb_test.drop(columns=['id', 'price'])

    return xgb_X, xgb_y, xgb_test


def get_xgbreg(X_train, y_train):
    xgb_reg = xgb.XGBRegressor(
                                learning_rate=0.03,
                                max_depth=7,
                                n_estimators=1100,
                                colsample_bytree=0.8,
                                # colsample_bylevel=0.9,
                                # colsample_bynode=0.8,
                                subsample=0.7,
                                alpha=1,
                                gamma=0.1,
                                )
    xgb_reg.fit(X_train, y_train)
    return xgb_reg


def xgb_kmeans_prediction(xgb_X, xgb_y, xgb_test, y_test_hat, nfolds=3):
    print('\nXgb cluster-based predictions...')
    y_test_hat['xgb_kmeaned_oof'] = np.zeros(xgb_test.shape[0])

    kmeans = xgb_X['kmeans'].unique()

    for centroid in kmeans:
        print(f'\nCluster number: {centroid}')

        kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

        print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

        kmeaned_X = xgb_X[xgb_X.kmeans == centroid]
        kmeaned_y = xgb_y.loc[kmeaned_X.index]
        kmeaned_test = xgb_test[xgb_test.kmeans == centroid]

        for train_index, valid_index in kfold.split(X=kmeaned_X, y=pd.qcut(kmeaned_y, 10, labels=False, duplicates='drop')):
            xgb_reg = get_xgbreg(kmeaned_X.iloc[train_index], kmeaned_y.iloc[train_index])

            y_train_hat = xgb_reg.predict(kmeaned_X.iloc[train_index])
            y_valid_hat = xgb_reg.predict(kmeaned_X.iloc[valid_index])
            y_test_hat.loc[kmeaned_test.index, 'xgb_kmeaned_oof'] += xgb_reg.predict(kmeaned_test)

            print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
            print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

        y_test_hat.loc[kmeaned_test.index, ['xgb_kmeaned']] = get_xgbreg(kmeaned_X, kmeaned_y).predict(kmeaned_test)

    y_test_hat['xgb_kmeaned'] = (xgb_test['area_total'] * np.exp(y_test_hat['xgb_kmeaned'])).round(1)
    y_test_hat['xgb_kmeaned_oof'] = (xgb_test['area_total'] * np.exp((y_test_hat['xgb_kmeaned_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat


def xgb_simple_prediction(xgb_X, xgb_y, xgb_test, y_test_hat, nfolds=3):
    print('\nXgb common predictions...')
    y_test_hat['xgb_oof'] = np.zeros(xgb_test.shape[0])

    kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

    print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

    for train_index, valid_index in kfold.split(X=xgb_X, y=pd.qcut(xgb_y, 100, labels=False, duplicates='drop')):
        xgb_reg = get_xgbreg(xgb_X.iloc[train_index], xgb_y.iloc[train_index])

        y_train_hat = xgb_reg.predict(xgb_X.iloc[train_index])
        y_valid_hat = xgb_reg.predict(xgb_X.iloc[valid_index])
        y_test_hat['xgb_oof'] += xgb_reg.predict(xgb_test)

        print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(xgb_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
        print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(xgb_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

    y_test_hat['xgb'] = get_xgbreg(xgb_X, xgb_y).predict(xgb_test)

    y_test_hat['xgb'] = (xgb_test['area_total'] * np.exp(y_test_hat['xgb'])).round(1)
    y_test_hat['xgb_oof'] = (xgb_test['area_total'] * np.exp((y_test_hat['xgb_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat

