# Two clusters
def add_kmeans(data_in):
    data = data_in.copy()
    
    data['kmeans'] = 7

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.8352) & 
             (data.latitude > 55.8258) & 
             (data.longitude > 37.4629) & 
             (data.longitude < 37.4743),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7623) & 
             (data.latitude > 55.7560) & 
             (data.longitude > 37.4415) & 
             (data.longitude < 37.4573),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7291) & 
             (data.latitude > 55.7125) & 
             (data.longitude > 37.4733) & 
             (data.longitude < 37.4832),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7278) & 
             (data.latitude > 55.7079) & 
             (data.longitude > 37.4929) & 
             (data.longitude < 37.5160),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7068) & 
             (data.latitude > 55.7017) & 
             (data.longitude > 37.4875) & 
             (data.longitude < 37.4965),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6558) & 
             (data.latitude > 55.6516) & 
             (data.longitude > 37.47) & 
             (data.longitude < 37.478),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7648) & 
             (data.latitude > 55.7601) & 
             (data.longitude > 37.5452) & 
             (data.longitude < 37.555),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7698) & 
             (data.latitude > 55.7297) & 
             (data.longitude > 37.5760) & 
             (data.longitude < 37.6287),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7509) & 
             (data.latitude > 55.7115) & 
             (data.longitude > 37.5555) & 
             (data.longitude < 37.5920),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7356) & 
             (data.latitude > 55.7119) & 
             (data.longitude > 37.5215) & 
             (data.longitude < 37.5403),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6958) & 
             (data.latitude > 55.6932) & 
             (data.longitude > 37.5258) & 
             (data.longitude < 37.5354),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7068) & 
             (data.latitude > 55.7014) & 
             (data.longitude > 37.5464) & 
             (data.longitude < 37.5652),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7352) & 
             (data.latitude > 55.7302) & 
             (data.longitude > 37.6253) & 
             (data.longitude < 37.6463),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7523) & 
             (data.latitude > 55.7464) & 
             (data.longitude > 37.5316) & 
             (data.longitude < 37.5423),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7603) & 
             (data.latitude > 55.75) & 
             (data.longitude > 37.5261) & 
             (data.longitude < 37.5751),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7179) & 
             (data.latitude > 55.6961) & 
             (data.longitude > 37.4961) & 
             (data.longitude < 37.5279),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7118) & 
             (data.latitude > 55.7029) & 
             (data.longitude > 37.565) & 
             (data.longitude < 37.5924),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7328) & 
             (data.latitude > 55.7096) & 
             (data.longitude > 37.5795) & 
             (data.longitude < 37.6341),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7795) & 
             (data.latitude > 55.7212) & 
             (data.longitude > 37.5595) & 
             (data.longitude < 37.6572),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.766) & 
             (data.latitude > 55.7318) & 
             (data.longitude > 37.6536) & 
             (data.longitude < 37.6685),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7821) & 
             (data.latitude > 55.7651) & 
             (data.longitude > 37.6541) & 
             (data.longitude < 37.6772),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7896) & 
             (data.latitude > 55.7779) & 
             (data.longitude > 37.5556) & 
             (data.longitude < 37.6445),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.81143) & 
             (data.latitude > 55.80557) & 
             (data.longitude > 37.44693) & 
             (data.longitude < 37.45355),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7768) & 
             (data.latitude > 55.7675) & 
             (data.longitude > 37.4613) & 
             (data.longitude < 37.4778),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7719) & 
             (data.latitude > 55.7661) & 
             (data.longitude > 37.3945) & 
             (data.longitude < 37.4028),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7821) & 
             (data.latitude > 55.775) & 
             (data.longitude > 37.4352) & 
             (data.longitude < 37.4443),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.74893) & 
             (data.latitude > 55.73314) & 
             (data.longitude > 37.5144) & 
             (data.longitude < 37.5634),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7773) & 
             (data.latitude > 55.7532) & 
             (data.longitude > 37.5384) & 
             (data.longitude < 37.5717),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.721) & 
             (data.latitude > 55.7067) & 
             (data.longitude > 37.5943) & 
             (data.longitude < 37.6379),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.69266) & 
             (data.latitude > 55.68733) & 
             (data.longitude > 37.52732) & 
             (data.longitude < 37.53313),
            'kmeans'] = 0

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7966) & 
             (data.latitude > 55.7898) & 
             (data.longitude > 37.4462) & 
             (data.longitude < 37.4508),
            'kmeans'] = 0

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.7828) & 
             (data.latitude > 55.7716) & 
             (data.longitude > 37.5385) & 
             (data.longitude < 37.5628),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.70641) & 
             (data.latitude > 55.7022) & 
             (data.longitude > 37.5769) & 
             (data.longitude < 37.5879),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.710798) & 
             (data.latitude > 55.70744) & 
             (data.longitude > 37.59754) & 
             (data.longitude < 37.6071),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.7914) & 
             (data.latitude > 55.78627) & 
             (data.longitude > 37.5547) & 
             (data.longitude < 37.5786),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.7887) & 
             (data.latitude > 55.781) & 
             (data.longitude > 37.5537) & 
             (data.longitude < 37.5671),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.70977) & 
             (data.latitude > 55.70814) & 
             (data.longitude > 37.60839) & 
             (data.longitude < 37.62605),
            'kmeans'] = 7

    data.loc[(data.kmeans==0) & 
             (data.latitude < 55.71141) & 
             (data.latitude > 55.71003) & 
             (data.longitude > 37.61429) & 
             (data.longitude < 37.6177),
            'kmeans'] = 7
    
    return data

# Three clusters
def add_midrange(data_in):
    
    data = data_in.copy()
    
    if 'kmeans' not in data.columns:
        data = add_kmeans(data)
    
    data.loc[(data.kmeans==7) & 
         (data.longitude > 37.7483),
        'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude > 55.8265),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.63212),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.longitude < 37.3915),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7566) & 
             (data.longitude > 37.7007),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6767) & 
             (data.longitude > 37.5831),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6564) & 
             (data.longitude > 37.5526),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6842) & 
             (data.longitude < 37.435),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude > 55.6503) & 
             (data.longitude < 37.4432),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude > 55.7872) & 
             (data.longitude < 37.4432),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude > 55.8201) & 
             (data.longitude < 37.4591),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) &
             (data.longitude > 37.7068),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude > 55.8045) & 
             (data.longitude > 37.6823),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) &
             (data.latitude < 55.6551),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.7105) & 
             (data.longitude < 37.4678),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.6716) & 
             (data.longitude < 37.5018),
            'kmeans'] = 3

    data.loc[(data.kmeans==7) & 
             (data.latitude < 55.69626) & 
             (data.longitude > 37.625),
            'kmeans'] = 3
    
    return data

