import numpy as np
import pandas as pd
from data_filtering import integer, real
import lightgbm as lgb
from sklearn.model_selection import StratifiedKFold
from utils import root_mean_squared_log_error


def lgb_preparation(train_data_in, test_data_in):
    train = train_data_in.copy()
    lgb_X = train.drop(columns=['id', 'price'])
    lgb_y = np.log(train.price / train.area_total)

    lgb_test = test_data_in.copy()
    lgb_test = lgb_test.drop(columns=['id', 'price'])

    feature_scale = [feature for feature in lgb_X.columns if feature not in integer+real]
    for feature in feature_scale:
        lgb_X[feature] = pd.Series(lgb_X[feature], dtype="category")
        lgb_test[feature] = pd.Series(lgb_test[feature], dtype="category")

    return lgb_X, lgb_y, lgb_test


def get_lgbreg(X_train, y_train):

    params = {
        # 'max_depth': 6,
        'boosting_type': 'gbdt',
        'objective': 'regression',
        'metric': {'l2'},
        'num_leaves': 27,
        'learning_rate': 0.03,
        'feature_fraction': 0.8,
        'bagging_fraction': 0.7,
        'bagging_freq': 5,
        'verbose': -1,
        'force_col_wise': 'True',
        'lambda_l1': 2,
        'lambda_l2': 1,
        # 'min_data_in_leaf': 90,
        # 'max_bin': 512,
    }

    lgb_train = lgb.Dataset(X_train, y_train, free_raw_data=False)

    light_gb_reg = lgb.train(
                            params,
                            lgb_train,
                            num_boost_round=900,
                            )
    return light_gb_reg


def lgb_kmeans_prediction(lgb_X, lgb_y, lgb_test, y_test_hat, nfolds=3):
    print('\nLgb cluster-based predictions...')
    y_test_hat['lgb_kmeaned_oof'] = np.zeros(lgb_test.shape[0])

    kmeans = lgb_X['kmeans'].unique()

    for centroid in kmeans:
        print(f'\nCluster number: {centroid}')

        kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

        print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

        kmeaned_X = lgb_X[lgb_X.kmeans == centroid]
        kmeaned_y = lgb_y.loc[kmeaned_X.index]
        kmeaned_test = lgb_test[lgb_test.kmeans == centroid]

        for train_index, valid_index in kfold.split(X=kmeaned_X, y=pd.qcut(kmeaned_y, 10, labels=False, duplicates='drop')):
            lgb_reg = get_lgbreg(kmeaned_X.iloc[train_index], kmeaned_y.iloc[train_index])

            y_train_hat = lgb_reg.predict(kmeaned_X.iloc[train_index])
            y_valid_hat = lgb_reg.predict(kmeaned_X.iloc[valid_index])
            y_test_hat.loc[kmeaned_test.index, 'lgb_kmeaned_oof'] += lgb_reg.predict(kmeaned_test)

            print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
            print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

        y_test_hat.loc[kmeaned_test.index, ['lgb_kmeaned']] = get_lgbreg(kmeaned_X, kmeaned_y).predict(kmeaned_test)

    y_test_hat['lgb_kmeaned'] = (lgb_test['area_total'] * np.exp(y_test_hat['lgb_kmeaned'])).round(1)
    y_test_hat['lgb_kmeaned_oof'] = (lgb_test['area_total'] * np.exp((y_test_hat['lgb_kmeaned_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat


def lgb_simple_prediction(lgb_X, lgb_y, lgb_test, y_test_hat, nfolds=3):
    print('\nLgb common predictions...')
    y_test_hat['lgb_oof'] = np.zeros(lgb_test.shape[0])

    kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

    print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

    for train_index, valid_index in kfold.split(X=lgb_X, y=pd.qcut(lgb_y, 100, labels=False, duplicates='drop')):
        lgb_reg = get_lgbreg(lgb_X.iloc[train_index], lgb_y.iloc[train_index])

        y_train_hat = lgb_reg.predict(lgb_X.iloc[train_index])
        y_valid_hat = lgb_reg.predict(lgb_X.iloc[valid_index])
        y_test_hat['lgb_oof'] += lgb_reg.predict(lgb_test)

        print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(lgb_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
        print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(lgb_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

    y_test_hat['lgb'] = get_lgbreg(lgb_X, lgb_y).predict(lgb_test)

    y_test_hat['lgb'] = (lgb_test['area_total'] * np.exp(y_test_hat['lgb'])).round(1)
    y_test_hat['lgb_oof'] = (lgb_test['area_total'] * np.exp((y_test_hat['lgb_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat

