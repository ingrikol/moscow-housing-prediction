from data_filtering import filter_data, fix_wrong_street_coordinates, fix_missing_coordinates
from distance_calculations import calculate_all_distances
from xgboost_algorithms import *
from light_gbm_algorithms import *
from kmeans import *
from catboost_algorithms import *
import pandas as pd
import numpy as np


def init():
    pd.pandas.set_option('display.max_columns', None)
    np.random.seed(42)


def load_raw_data():
    apart_train = pd.read_csv('data/apartments_train.csv')
    build_train = pd.read_csv('data/buildings_train.csv')
    train = pd.merge(apart_train, build_train.set_index('id'), how='left', left_on='building_id', right_index=True)

    apart_test = pd.read_csv('data/apartments_test.csv')
    build_test = pd.read_csv('data/buildings_test.csv')
    test = pd.merge(apart_test, build_test.set_index('id'), how='left', left_on='building_id', right_index=True)

    data = pd.concat([train, test])
    return data


def load_calculated_data(string):
    return pd.read_csv(string)

def main():
    init()
    #data = load_raw_data()
    data = load_calculated_data('data/data_calculated_all.csv')

    #data = fix_missing_coordinates(data)
    #data = fix_wrong_street_coordinates(data)

    # Calculate distances
    #data = calculate_all_distances(data)

    # Save data to CSV
    #data.to_csv('data/data_calculated_all.csv', index=False)

    # Filter data
    data = filter_data(data)
    data = add_midrange(data)

    # Split to train and test set
    train_data = data[data.price.notna()]
    test_data = data[data.price.isna()]

    y_test_hat = pd.DataFrame(data=test_data['id'], index=test_data.index, columns=['id'])

    # XGBOOST Prediction
    #xgb_X, xgb_y, xgb_test = xgb_preparation(train_data, test_data)
    #y_test_hat = xgb_kmeans_prediction(xgb_X, xgb_y, xgb_test, y_test_hat, nfolds=10)
    #y_test_hat = xgb_simple_prediction(xgb_X, xgb_y, xgb_test, y_test_hat, nfolds=3)

    # LIGHT GBM
    lgb_X, lgb_y, lgb_test = lgb_preparation(train_data, test_data)
    #y_test_hat = lgb_kmeans_prediction(lgb_X, lgb_y, lgb_test, y_test_hat)
    y_test_hat = lgb_simple_prediction(lgb_X, lgb_y, lgb_test, y_test_hat)

    # CATBOOST
    catboost_X, catboost_y, catboost_test = catboost_preparation(train_data, test_data)
    #y_test_hat = catboost_kmeans_prediction(catboost_X, catboost_y, catboost_test, y_test_hat, nfolds=10)
    y_test_hat = catboost_simple_prediction(catboost_X, catboost_y, catboost_test, y_test_hat, nfolds=3)
    print(y_test_hat)

    # SUBMISSION
    #submission = pd.DataFrame(data=test_data['id'])
    #submission['price_prediction'] = test_data['mean50']
    #submission[['id', 'price_prediction']].to_csv('mean.csv', index=False)

    return y_test_hat


main()

