import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans


def root_mean_squared_log_error(y_true, y_pred):
    assert (y_true >= 0).all()
    assert (y_pred >= 0).all()
    log_error = np.log1p(y_pred) - np.log1p(y_true)
    return np.mean(log_error ** 2) ** 0.5


def get_highest_RMSLE(y_true, y_pred):
    assert (y_true >= 0).all()
    assert (y_pred >= 0).all()
    log_error = np.log1p(y_pred) - np.log1p(y_true)
    ret = pd.DataFrame(data=log_error, columns=['RMSLE'])
    ret['RMSLE'] = (log_error ** 2) ** 0.5
    ret['pred'] = y_pred.round(1)
    ret['true'] = y_true.round(1)
    ret = ret[ret['RMSLE'] > 0.15]
    return ret


def plot_map(data, q_lo=0.0, q_hi=0.9, cmap='autumn', column='price', title='Moscow apartment price by location'):
    train_data = data.loc[~data.price.isna(), ['latitude', 'longitude', column]].sort_values(by=column, ascending=True)
    test_data = data.loc[data.price.isna(), ['latitude', 'longitude', column]]
    backdrop = plt.imread('data/moscow.png')
    backdrop = np.einsum('hwc, c -> hw', backdrop, [0, 1, 0, 0]) ** 2
    plt.figure(figsize=(12, 8), dpi=100)
    ax = plt.gca()
    hue_norm = plt.Normalize(0, 100000000)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(0, 100000000))
    sm.set_array([])
    ax.imshow(backdrop, alpha=0.5, extent=[37, 38, 55.5, 56], aspect='auto', cmap='bone', norm=plt.Normalize(0.0, 2))
    sns.scatterplot(x='longitude', y='latitude', hue=train_data[column].tolist(), ax=ax, s=5, alpha=1, palette=cmap,linewidth=0, hue_norm=hue_norm, data=train_data)
    sns.scatterplot(x='longitude', y='latitude', color='k', ax=ax, s=7, alpha=1, linewidth=0, data=test_data)
    # ax.set_xlim(37, 38)    # min/max longitude of image 
    # ax.set_ylim(55.5, 56)  # min/max latitude of image
    ax.legend().remove()
    ax.figure.colorbar(sm)
    ax.set_title(title)
    return ax, hue_norm

def plot_kmeans(data_input, s=20):
    data = data_input.copy()
    data = data[~(data.latitude.isna() | data.longitude.isna())]
    backdrop = plt.imread('data/moscow.png')
    backdrop = np.einsum('hwc, c -> hw', backdrop, [0, 1, 0, 0]) ** 2
    plt.figure(figsize=(12, 8), dpi=100)
    ax = plt.gca()
    ax.imshow(backdrop, alpha=0.5, extent=[37, 38, 55.5, 56], aspect='auto', cmap='bone', norm=plt.Normalize(0.0, 2))
    sns.scatterplot(x=data['longitude'].values, y=data['latitude'].values, c=data['kmeans'].round(0).tolist(), palette='autumn', ax=ax, s=5, alpha=0.75, linewidth=0, data=data)
    return ax
