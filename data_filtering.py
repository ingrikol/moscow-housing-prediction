import numpy as np
import pandas as pd

categorical = ['seller', 'condition', 'district', 'material','parking', 'heating', 'windows_court', 'windows_street', 'garbage_chute', 'constructed']
integer = ['floor', 'rooms', 'bathrooms_shared', 'bathrooms_private','balconies', 'loggias', 'stories', 'n_in_build']
real = ['area_total', 'area_kitchen', 'area_living', 'center_dist', 'latitude', 'longitude', 'airport_dist', 'station_dist', 'floor_stories']
address = ['street']


def fix_missing_coordinates(data_in):
    data = data_in.copy()
    data.loc[data.id == 30244, ['latitude', 'longitude']] = [55.544624, 37.479758]
    data.loc[data.id == 31881, ['latitude', 'longitude']] = [55.544624, 37.479758]
    data.loc[data.id == 28375, ['latitude', 'longitude']] = [55.544624, 37.479758]
    data.loc[data.id == 25796, ['latitude', 'longitude']] = [55.544624, 37.479758]
    data.loc[data.id == 28004, ['latitude', 'longitude']] = [55.809201, 37.349888]
    data.loc[data.id == 32832, ['latitude', 'longitude']] = [55.614371, 37.398489]
    data.loc[data.id == 25814, ['latitude', 'longitude']] = [55.627692, 37.464994]
    data.loc[data.id == 23308, ['latitude', 'longitude']] = [55.573862, 37.488257]
    data.loc[data.id == 23375, ['latitude', 'longitude']] = [55.573862, 37.488257]
    data.loc[data.id == 13613, ['latitude', 'longitude']] = [55.603252, 37.167907]
    data.loc[data.id == 7561,  ['latitude', 'longitude']] = [55.500278, 37.515187]
    return data


def fix_wrong_street_coordinates(data_in):
    data = data_in.copy()
    data.loc[(data.street == 'улица Троицкая'), ['latitude', 'longitude', 'district']] = [55.500710, 37.514676, np.NaN]
    data.loc[(data.street == 'улица Пионерская'), ['latitude', 'longitude', 'district']] = [55.667793, 37.947628, np.NaN]
    data.loc[(data.street == 'улица Лесная'), ['latitude', 'longitude', 'district']] = [55.524014, 37.215442, np.NaN]
    data.loc[(data.street == '10-я фаза тер.'), ['latitude', 'longitude', 'district']] = [55.603057, 37.375603, np.NaN]
    data.loc[(data.street == 'улица Городская'), ['latitude', 'longitude', 'district']] = [55.491955, 37.314998, np.NaN]
    data.loc[(data.street == 'Саларьево Парк ЖК'), ['latitude', 'longitude', 'district']] = [55.617025, 37.413747, np.NaN]
    data.loc[(data.street == 'улица Чехова'), ['latitude', 'longitude', 'district']] = [55.610799, 37.206002, np.NaN]
    data.loc[(data.street == 'м. Селигерская') & (data.address == 'Селигер Сити ЖК'), ['latitude', 'longitude', 'district']] = [55.866721, 37.545108, np.NaN]
    data.loc[(data.street == 'улица 2-я Линия'), ['latitude', 'longitude', 'district']] = [55.629547, 37.465136, np.NaN]
    data.loc[(data.street == 'улица Горького'), ['latitude', 'longitude', 'district']] = [55.659155, 37.326868, np.NaN]
    data.loc[(data.street == 'улица Пушкинская'), ['latitude', 'longitude', 'district']] = [55.505873, 37.562631, np.NaN]
    data.loc[(data.street == 'улица Верхняя'), ['latitude', 'longitude', 'district']] = [55.632292, 37.427476, np.NaN]
    return data


def remove_highest_RMSLE_rows(data_in):
    data = data_in.copy()
    data = data[~(data.id.isin([11247, 18888, 16427, 10443,  7572, 17457,
                             17508, 8308, 13751, 16562, 23196,  2363, 10663, 17373,
                             21532, 13207, 8047, 13206, 20329, 16793,   860, 11148,
                             3372, 15904, 11711, 16492, 14556,  2999
                           ]))]
    return data


def bin_constructed_to_fewer_categories(data_in):
    data = data_in.copy()
    data['constructed'] = pd.cut(data['constructed'], bins=[0, 1900, 1930, 1950, 1962, 1967, 1971, 1980,
                                                        1989, 1995, 1999, 2002, 2005, 2006, 2007, 2008,
                                                        2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016,
                                                        2017, 2018, 2019, 2020, 2021, 2022, 2050], labels=False)
    return data


def estimate_area_living(data):
    data.loc[(data['area_living'] == 80) & (data['area_kitchen'] == 20), ['area_living', 'area_kitchen']] = [np.NaN,
                                                                                                             np.NaN]
    data.loc[(data['area_living'] == 30.3) & (data['area_kitchen'] == 8.2), ['area_living', 'area_kitchen']] = [np.NaN,
                                                                                                                np.NaN]
    data.loc[(data['area_living'] == 0), ['area_living']] = [np.NaN]

    data['area_ratio'] = np.where(data['area_total'] > data['area_living'], data['area_living']/data['area_total'], np.NaN)

    # Assuming that area_living is wrong
    average_ratio = np.mean(data['area_ratio'])
    data['area_living'] = np.where(data['area_total'] < data['area_living'], data['area_living'] * average_ratio, data['area_living'])
    return data


def fix_areas(data_in):
    data = data_in.copy()

    data.loc[(data['area_living'] == 80) & (data['area_kitchen'] == 20), ['area_living', 'area_kitchen']] = [np.NaN, np.NaN]
    data.loc[(data['area_living'] == 30.3) & (data['area_kitchen'] == 8.2), ['area_living', 'area_kitchen']] = [np.NaN, np.NaN]
    data.loc[(data['area_living'] == 0), ['area_living']] = [np.NaN]

    data.loc[data.area_living > data.area_total * 0.99, ['area_living']] = [np.NaN]
    data.loc[data.area_living < data.area_total * 0.4, ['area_living']] = [np.NaN]  # OR: data = data.drop(columns='area_living')
    data.loc[data.area_kitchen > data.area_total * 0.5, ['area_kitchen']] = [np.NaN]

    data.loc[(data['area_total'] > 120) & (data['rooms'] == 1), ['rooms']] = [np.NaN]
    data['area_living'] = data['area_living'] / data['rooms']
    data.loc[(data['area_living'] > 90), ['area_living', 'rooms']] = [np.NaN, np.NaN]
    return data


def fix_err_floor_stories(data_in):
    data = data_in.copy()
    data.loc[data['floor'] > data['stories'], ['stories']] = data.loc[data['floor'] > data['stories'], ['floor']]
    data['floor_stories'] = (data['floor'] / data['stories']).round(2)
    return data


def fix_street_name(data_in):
    data = data_in.copy()
    data['street'] = data['street'].str.replace('улица', '').str.strip()
    data['street'] = pd.factorize(data['street'])[0] + 1
    return data


def get_num_in_building(data_in):
    data = data_in.copy()
    data['n_in_build'] = data.groupby('building_id')['building_id'].transform('count')
    return data


def merge_balconies_and_loggias(data_in):
    data = data_in.copy()
    data.loc[(data.balconies.isna()) & (~(data.loggias.isna())), ['balconies']] = [0]
    data.loc[(~(data.balconies.isna())) & (data.loggias.isna()), ['loggias']] = [0]
    data['balconies'] = data['balconies'] + data['loggias']
    return data


def filter_data(data_in):
    data = data_in.copy()
    data = fix_missing_coordinates(data)
    data = fix_wrong_street_coordinates(data)
    #data = remove_highest_RMSLE_rows(data)
    data = bin_constructed_to_fewer_categories(data)
    #data = fix_areas(data)
    data = estimate_area_living(data)
    data = fix_err_floor_stories(data)
    data = fix_street_name(data)
    data = get_num_in_building(data)
    data = merge_balconies_and_loggias(data)

    # Drop columns
    data = data.drop(
        columns=['address', 'layout', 'ceiling', 'phones', 'building_id', 'elevator_without',
                 'elevator_service', 'new', 'loggias'])

    return data

