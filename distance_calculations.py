import pandas as pd
from haversine import haversine


def haversine_distance(latitude_1, latitude_2, longitude_1, longitude_2):
    return haversine((latitude_1, longitude_1), (latitude_2, longitude_2))


def find_nearest(latitude, longitude, locations):
    min_dist = float('inf')
    for index, location in locations.iterrows():
        dist = haversine_distance(latitude, location.lat, longitude, location.lon)
        if dist < min_dist:
            min_dist = dist
    return min_dist


def calculate_all_distances(data):
    airports = pd.read_csv('data/airports.csv')
    parks = pd.read_csv('data/parks.csv')
    universities = pd.read_csv('data/universities.csv')
    stations = pd.read_csv('data/stations.csv')

    for index, row in data.iterrows():
        data.loc[data.id == row.id, ['financial_center_dist']] = haversine_distance(row['latitude'], 55.746667, row['longitude'], 37.536944)
        data.loc[data.id == row.id, ['center_dist']] = haversine_distance(row['latitude'], 55.751244, row['longitude'], 37.618423)
        data.loc[data.id == row.id, ['station_dist']] = find_nearest(row.latitude, row.longitude, stations)
        data.loc[data.id == row.id, ['uni_dist']] = find_nearest(row.longitude, row.latitude, universities)
        data.loc[data.id == row.id, ['park_dist']] = find_nearest(row.longitude, row.latitude, parks)
        data.loc[data.id == row.id, ['airport_dist']] = find_nearest(row.latitude, row.longitude, airports)

    return data