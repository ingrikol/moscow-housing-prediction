from catboost import CatBoostRegressor, FeaturesData, Pool
import numpy as np
import pandas as pd
from data_filtering import integer, real
from sklearn.model_selection import StratifiedKFold
from utils import root_mean_squared_log_error


def catboost_preparation(train_data_in, test_data_in):
    train = train_data_in.copy()
    catboost_X = train.drop(columns=['id', 'price'])
    catboost_y = np.log(train.price / train.area_total)

    catboost_test = test_data_in.copy()
    catboost_test = catboost_test.drop(columns=['id', 'price'])

    feature_scale = [feature for feature in catboost_X.columns if feature not in integer+real]

    for feature in feature_scale:
        catboost_X[feature] = catboost_X[feature].fillna(-1)
        catboost_test[feature] = catboost_test[feature].fillna(-1)

        catboost_X[feature] = pd.Series(catboost_X[feature], dtype="category").astype('int32')
        catboost_test[feature] = pd.Series(catboost_test[feature], dtype="category").astype('int32')

    return catboost_X, catboost_y, catboost_test


def get_catboost_reg(X_train, y_train):
    feature_scale = [feature for feature in X_train.columns if feature not in integer + real]
    model = CatBoostRegressor(iterations=1000, learning_rate=0.05, depth=8)
    # Fit model
    model.fit(X_train, y_train, cat_features=feature_scale, verbose=0)
    return model


def catboost_kmeans_prediction(catboost_X, catboost_y, catboost_test, y_test_hat, nfolds=3):
    print('\nCatboost cluster-based predictions...')
    y_test_hat['catboost_kmeaned_oof'] = np.zeros(catboost_test.shape[0])

    kmeans = catboost_X['kmeans'].unique()
    print("kemeans: " + str(kmeans))
    for centroid in kmeans:
        print(f'\nCluster number: {centroid}')

        kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

        print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

        kmeaned_X = catboost_X[catboost_X.kmeans == centroid]
        kmeaned_y = catboost_y.loc[kmeaned_X.index]
        kmeaned_test = catboost_test[catboost_test.kmeans == centroid]

        for train_index, valid_index in kfold.split(X=kmeaned_X, y=pd.qcut(kmeaned_y, 10, labels=False, duplicates='drop')):
            catboost_reg = get_catboost_reg(kmeaned_X.iloc[train_index], kmeaned_y.iloc[train_index])

            y_train_hat = catboost_reg.predict(kmeaned_X.iloc[train_index])
            y_valid_hat = catboost_reg.predict(kmeaned_X.iloc[valid_index])
            y_test_hat.loc[kmeaned_test.index, 'catboost_kmeaned_oof'] += catboost_reg.predict(kmeaned_test)

            print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
            print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(kmeaned_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

        y_test_hat.loc[kmeaned_test.index, ['catboost_kmeaned']] = get_catboost_reg(kmeaned_X, kmeaned_y).predict(kmeaned_test)

    y_test_hat['catboost_kmeaned'] = (catboost_test['area_total'] * np.exp(y_test_hat['catboost_kmeaned'])).round(1)
    y_test_hat['catboost_kmeaned_oof'] = (catboost_test['area_total'] * np.exp((y_test_hat['catboost_kmeaned_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat


def catboost_simple_prediction(catboost_X, catboost_y, catboost_test, y_test_hat, nfolds=3):
    print('\nCatboost common predictions...')
    y_test_hat['catboost_oof'] = np.zeros(catboost_test.shape[0])

    kfold = StratifiedKFold(n_splits=nfolds, shuffle=True, random_state=18)

    print(f'Performing {kfold.get_n_splits()}-fold split validation...\n')

    # Splitting test data into train and validation sets
    for train_index, valid_index in kfold.split(X=catboost_X, y=pd.qcut(catboost_y, 100, labels=False, duplicates='drop')):
        catboost_reg = get_catboost_reg(catboost_X.iloc[train_index], catboost_y.iloc[train_index])

        y_train_hat = catboost_reg.predict(catboost_X.iloc[train_index])
        y_valid_hat = catboost_reg.predict(catboost_X.iloc[valid_index])
        y_test_hat['catboost_oof'] += catboost_reg.predict(catboost_test)

        print(f'Train RMSLE: {root_mean_squared_log_error(y_true=np.exp(catboost_y.iloc[train_index]), y_pred=np.exp(y_train_hat)) :.4f}')
        print(f'Valid RMSLE: {root_mean_squared_log_error(y_true=np.exp(catboost_y.iloc[valid_index]), y_pred=np.exp(y_valid_hat)) :.4f}\n')

    # Predicting test data on the whole training set
    y_test_hat['catboost'] = get_catboost_reg(catboost_X, catboost_y).predict(catboost_test)

    y_test_hat['catboost'] = (catboost_test['area_total'] * np.exp(y_test_hat['catboost'])).round(1)
    y_test_hat['catboost_oof'] = (catboost_test['area_total'] * np.exp((y_test_hat['catboost_oof'] / kfold.get_n_splits()))).round(1)
    return y_test_hat


